import Vue from "vue";
import Router from "vue-router";

const routerOptions = [
  { path: "/", component: "Landing" },
  { path: "/signin", component: "SignIn" },
  { path: "/logout", component: "LogOut" },
  { path: "/signup", component: "SignUp" },
  { path: "/vereine", component: "MainList", name:"Vereine", meta: { requiresAuth: true } },
  { path: "/bestenliste", component: "MainList", name:"Bestenliste", meta: { requiresAuth: true } },
  { path: "/kontakte", component: "MainList", name:"Kontakte", meta: { requiresAuth: true } },
  { path: "*", component: "NotFound" }
];

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`../components/${route.component}.vue`),
  };
});

Vue.use(Router);

export default new Router({
  mode: "history",
  routes
});
